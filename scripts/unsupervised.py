#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Unsupervised topic extraction based on closed-source* Twitter corpus.
Short warning: It uses the spaCy library, which takes up some RAM.

* As of yet ;)
"""
# standard imports
import collections
import math
import os
import re

# global imports
import nltk
import spacy

# local imports


# Custom tweet data structure
Tweet = collections.namedtuple("Tweet", ("author", "date", "spectrum", "text"))
# German spaCy instance
nlp = spacy.load("de")
# Regular Expression Parser, patterns based on TIGER Annotationsschema
regexp_parser = nltk.RegexpParser('''
    ADV-PIAT-NN: {<ADV> <PIAT> <NN>}
    GEN-NP: {<ART>? <NN|NE>+ <APPR|APPRART|PPOSAT> <NN|NE>+}
    PP-NP: {<PIS|PPOSAT> <ADJA|ADV>? <NN|NE>+}
    ADJ-NP: {<ART>? <ADV|ADJA|ADJD>+ <ART>? <CARD>? <NN|NE>+}
    ''')
# Noun phrase expressions
noun_phrase_labels = {"ADJ-NP", "ADV-PIAT-NN", "GEN-NP", "PP-NP"}


def load_data(path):
    """Load data from full dataset."""
    with open(path, "r") as input_file:
        data_generator = (line.split("\t") for line in input_file.readlines())

    return data_generator


def generate_tweets(data_generator):
    """Generate Tweets from data generator."""
    tweet_generator = (Tweet(entry[5], entry[2], entry[0], entry[8])
                       for entry in data_generator)

    return tweet_generator


def generate_clean_tweets(tweet_generator):
    """Strip URLs and [NEWLINE] from text in tweet generator."""
    for tweet in tweet_generator:
        # Remove URLS
        clean_text = re.sub(r'https?:\/\/.*[\r\n]*', '', tweet.text)
        # Insert full stops before [NEWLINE]
        clean_text = re.sub(r'[^.]\[NEWLINE]\]', '\.\[NEWLINE\]', clean_text)
        # Remove [NEWLINE] tags
        clean_text = re.sub(r'\[NEWLINE\]', ' ', clean_text)
        # Remove Retweets
        clean_text = re.sub(r'^RT\b.*', '', clean_text)
        # Remove hashtags
        clean_text = re.sub(r'\#[a-zA-Z].*\b', '', clean_text)
        if clean_text:
            new_tweet = Tweet(tweet.author, tweet.date, tweet.spectrum,
                              clean_text)
            yield new_tweet


def generate_noun_phrases(tweet_generator):
    """Parse tweet text for noun phrases using regular expressions."""
    for tweet in tweet_generator:
        # Annotate POS tags
        word_pos_tag_pairs = [(token.orth_, token.tag_) for token in nlp(tweet.text)]
        # Parse POS tags using regexp parser
        tweet_parse_tree = regexp_parser.parse(word_pos_tag_pairs)
        # Iterate over all subtrees and yield noun phrases
        for subtree in tweet_parse_tree.subtrees():
            if subtree.label() in noun_phrase_labels:
                noun_phrase = " ".join(word for word, _ in subtree.leaves())
                yield noun_phrase


def count(generator):
    """Count elements in a generator."""
    counter = collections.Counter(generator)
    return counter


def generate_author_tweets_dictionary(tweet_generator):
    """Generate a dictionary of the form {author: [tweet texts]}."""
    author_tweets_dictionary = collections.defaultdict(list)
    for tweet in tweet_generator:
        author_tweets_dictionary[tweet.author].append(tweet.text)

    return author_tweets_dictionary


def compute_tf_idf_weights(noun_phrase_counter, author_tweets_dictionary):
    """Compute tf-idf (term frequency - inverse document frequency) weights for a profile based on ints noun phrase counter and the complete Tweet corpus."""
    tf_idf_weights = collections.defaultdict(float)
    _, normalization_factor = noun_phrase_counter.most_common(1)[0]
    number_of_profiles = len(author_tweets_dictionary.keys())

    for phrase in noun_phrase_counter.keys():
        tf_phrase = noun_phrase_counter[phrase] / normalization_factor
        containing_profiles = {author for author in author_tweets_dictionary.keys() if re.search(phrase, " ".join(author_tweets_dictionary[author]))}
        if containing_profiles:
            idf_phrase = math.log(number_of_profiles / len(containing_profiles))
            tf_idf_weights[phrase] = tf_phrase * idf_phrase

    return tf_idf_weights
