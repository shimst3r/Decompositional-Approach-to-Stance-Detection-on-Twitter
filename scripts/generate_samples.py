#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Take the jena.csv document containing annotated tweets and draw random
samples. The procedure is as follows:
    1. Load file using readlines() and split("\t").
    2. Filter resulting list for correct labels (i.e. L,R (,U)).
    3. Shuffle list and sort w.r.t. list element containing the label (i.e. index 0).
    4. Generate two index sets by sampling over number of resp. tweets.
    5. Filter tweet list using these index sets.
    6. Save resulting list to file using pickle.
"""
import argparse
import os.path
import pickle
import random

# Define values
sample_size = 500

# Generate command line argument parser, which will be used to locate jena.csv.
parser = argparse.ArgumentParser(description="Draw samples from jena.csv.")
parser.add_argument("in_path", type=str, help="Path to jena.csv")
parser.add_argument("out_path", type=str, help="Output path for filtered tweets")
args = parser.parse_args()

# Read file and filter for appropriate rows
with open(os.path.abspath(args.in_path), "r") as input_file:
  corpus = [line.split("\t")
            for line in input_file.readlines()
            if line.split("\t")[0] in ["L", "R"]]

# Shuffle and resort list - this ensures correct processing of data
random.shuffle(corpus)
corpus = sorted(corpus, key=lambda x: x[0])  # sorting by labels

# Find min and max index for both "L" and "R" labels
min_l = min([index for index, line in enumerate(corpus) if line[0] == "L"])
max_l = max([index for index, line in enumerate(corpus) if line[0] == "L"])
min_r = min([index for index, line in enumerate(corpus) if line[0] == "R"])
max_r = max([index for index, line in enumerate(corpus) if line[0] == "R"])

# Sample a set of 500 random integers each bounded by min_{l/r} and max_{l/r}.
indices_l = set(random.sample(range(min_l, max_l + 1), sample_size))
indices_r = set(random.sample(range(min_r, max_r + 1), sample_size))

# Filter tweets using index sets (set inclusions is faster than list inclusion)
samples = [line
           for index, line in enumerate(corpus)
           if index in indices_l
           or index in indices_r]

# Format samples s.t. it corresponds to the original format of jena.csv.
samples = ["\t".join(line) for line in samples]

# Write samples to file (as byte code, using pickle.dump)
with open(os.path.abspath(args.out_path), "wb") as output_file:
  pickle.dump(samples, output_file)
