#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Take a pickled subset of jena.csv data and turn it into a dictionary
of the form {label: [tweet_body]}.
"""

import argparse
import os
import pickle


def load_data(in_path):
    """ Read pickled file and turn it into a dictionary. """
    with open(os.path.abspath(in_path), "rb") as input_file:
        pickled_input = pickle.load(input_file)

    return pickled_input
