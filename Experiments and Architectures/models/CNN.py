import torch

import torch.nn as nn
import torch.nn.functional as F

class CNN(nn.Module):
    def __init__(
        self,
        vocab_size,
        embedding_dim,
        sent_len,
        output_size,
        kernel_dim=100,
        filter_sizes=(3,4,5),
        dropout=0.5):

        super(CNN, self).__init__()

        self.embedding = nn.Embedding(
            num_embeddings=vocab_size,
            embedding_dim=embedding_dim
        )

        self.convolution_layer = nn.ModuleList(
            [
                nn.Conv2d(
                    in_channels=1,
                    out_channels=kernel_dim,
                    kernel_size=(filter_size, embedding_dim),
                    stride=1
                )
                for filter_size in filter_sizes
            ]
        )

        self.dropout = nn.Dropout(dropout)

        self.fc = nn.Linear(
            kernel_dim*len(filter_sizes),
            output_size
        )

        self.log_softmax = nn.LogSoftmax(dim=0)

    def init_weights(self, word_vectors):
        self.embedding.weights = nn.Parameter(
            torch.from_numpy(word_vectors).float()
        )

        self.embedding.weights.requires_grad = False

    def forward(self, sentence, is_training=False):
        # (b_size, s_len) -> (b_size, s_len, emb_dim)
        x = self.embedding(sentence)

        # (b_size, s_len, emb_dim) -> (b_size, 1, s_len, emb_dim)
        x = x.unsqueeze(1)

        xs = []
        for conv in self.convolution_layer:
            # (b_size, 1, s_len, emb_dim) -> (b_size, k_dim, s_len-f_size+1, 1)
            x_bar = F.relu(conv(x)) 
            # (b_size, k_dim, s_len-f_size+1, 1) -> (b_size, k_dim, s_len-f_size+1)
            x_bar = x_bar.squeeze(3)
            # (b_size, k_dim, s_len-f_size+1, 1) -> (b_size, k_dim, s_len-f_size+1)
            x_bar = F.max_pool1d(x_bar, x_bar.size(2))
            x_bar = x_bar.squeeze(2)
            xs.append(x_bar)

        # (b_size, k_dim)^3 -> (b_size, 3*k_dim)
        x = torch.cat(xs, 1)

        # (b_size, 3*k_dim) -> (b_size, 3*k_dim)
        if is_training:
            x = self.dropout(x)

        # (b_size, 3*k_dim) -> (b_size, out_size)
        x = self.fc(x)

        # (b_size, out_size) -> (b_size, out_size)
        return self.log_softmax(x) 
