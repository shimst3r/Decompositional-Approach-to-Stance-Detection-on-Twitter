import json
import numpy as np
import pandas as pd
import re
import sqlite3
import torch

from nltk.tokenize import TweetTokenizer
from sqlite3 import DatabaseError
from torch.autograd import Variable
from torch.utils.data import DataLoader, Dataset


tokenizer = TweetTokenizer()

def remove_urls(text):
    """Removes Twitter short links of the form https://t.co/..."""
    result = re.sub(r'https://t.co/[^ ]+', '', text)
    
    return result

def map_target_to_label(target):
    """Maps a given target to the corresponding label."""
    return {
        "Gender Equality": 0,
        "Objectification of Women": 1,
        "Conservatism": 2
    }.get(target, 4)

def map_text_to_ascii_table(text):
    """Maps a given text to extended ASCII range."""
    result = " ".join(
        [
            "".join(
                [
                    character.lower()
                    for character in word
                    if ord(character) > 31
                    and ord(character) < 256
                    and character.strip() # removes blank characters from index
                    and not character.isdigit() # removes numbers, which aren't included in GloVe
                ]
            )

            for word in tokenizer.tokenize(text)
        ]
    )

    return result

def determine_max_length(task, target=None):
    """Returns the precomputed maximal sentence length for
    a given task (and possibly target)."""

    head = task.split('_')[0]

    if head == 'metoo':
        return 482
    elif head == 'target':
        return 948
    elif head == 'stance':
        if target == 'Objectification of Women':
            return 948
        elif target == 'Gender Equality':
            return 707
        elif target == 'Conservatism':
            return 912
        else:
            raise ValueError(f"Unknown target: {target}")
    else:
        raise ValueError(f"Unknown task: {task}")

def map_text_to_ascii_charlist_and_pad_it(text, max_len):
    """Maps a given text to a list of characters in the extended ASCII range and
    pads the result to a given max_len using B as placeholder."""
    result = [
        character.lower()
        for character in text
        if ord(character) > 31
        and ord(character) < 256
        and character.strip() # removes blank characters from index
        and not character.isdigit() # removes numbers, which aren't included in GloVe
    ]

    # Currently no padding, ha. :D
    # result += ['B'] * (max_len - len(result))

    return result

def build_word_index(root, series):
    """Builds a word index from a given series of texts. Hacked!"""
    with open(f'{root}/index.json', 'r', encoding='utf-8') as f:
        vocab = json.load(f)

    result = {
        idx: word
        for idx, word in enumerate(sorted(vocab))
    }

    return result

def load_embeddings(root, word_index):
    """Loads word embeddings located in root and indexed by word_index."""
    with open(f'{root}/embeddings.txt', 'r', encoding='utf-8')as f:
        data = f.readlines()

    embedding_dict = {
        line[0]: np.array(line[1:].split(), dtype=np.float64)
        for line in data
    }

    result = [
        embedding_dict[
            word_index[
                idx
            ]
        ]
        for idx in word_index
    ]

    return np.array(result)

def map_charlist_to_index(series, word_index):
    """Maps a given charlist to corresponding keys in word_index
    and convert it to Long Tensor."""
    word_to_index = {
        word: idx
        for idx, word in word_index.items()
    }

    result = series.apply(
        lambda text: torch.tensor(
            [
                word_to_index[word]
                for word in text
            ],
            dtype=torch.long
        )
    )

    return result

class MeTooDataset(Dataset):
    """#MeToo dataset."""

    def __init__(self, root, mode, task, target=None, time_frame=None):
        try:
            conn = sqlite3.connect(f'{root}/tweets.db')
        except:
            raise DatabaseError(f"No database found at {root}")

        if task == 'target_training':
            df = pd.read_sql(
                sql='select text, target from tweets',
                con=conn
            )

            self.X, self.y = df['text'], df['target'].apply(map_target_to_label)


        elif task == 'target_testing':
            # Ignoring 'None' target because it is currently not supported in machine learning algorithms.
            df = pd.read_sql(
                sql=('select text, target, user_id from profiles_oct_nov where target != "None" '
                     'union select text, target, user_id from profiles_dec_jan where target != "None" '
                     'union select text, target, user_id from profiles_feb_mar where target != "None" '
                     'union select text, target, user_id from trump_oct_nov where target != "None" '
                     'union select text, target, user_id from trump_dec_jan where target != "None" '
                     'union select text, target, user_id from trump_feb_mar where target != "None"'),
                con=conn
            )

            self.X, self.y, self.id = df['text'], df['target'].apply(map_target_to_label), df['user_id']
            
        elif task == 'target_testing_profiles':
            df = pd.read_sql(
                sql=('select text, target, user_id from profiles_oct_nov where target != "None" '
                     'union select text, target, user_id from profiles_dec_jan where target != "None" '
                     'union select text, target, user_id from profiles_feb_mar where target != "None" '),
                con=conn
            )

            self.X, self.y, self.id = df['text'], df['target'].apply(map_target_to_label), df['user_id']
            
        elif task == 'target_testing_trump':
            # Ignoring 'None' target because it is currently not supported in machine learning algorithms.
            df = pd.read_sql(
                sql=('select text, target, user_id from trump_oct_nov where target != "None" '
                     'union select text, target, user_id from trump_dec_jan where target != "None" '
                     'union select text, target, user_id from trump_feb_mar where target != "None"'),
                con=conn
            )

            self.X, self.y, self.id = df['text'], df['target'].apply(map_target_to_label), df['user_id']

        elif task == 'target_testing_over_time':
            if time_frame == None:
                raise ValueError("You need to specify a time frame.")
                
            if time_frame not in {'oct_nov', 'dec_jan', 'feb_mar'}:
                raise ValueError(f"{time_frame} is not a valid time frame.")
                
            # Ignoring 'None' target because it is currently not supported in machine learning algorithms.
            df = pd.read_sql(
                sql=(f'select text, target, user_id from profiles_{time_frame} where target != "None" '
                     f'union select text, target, user_id from trump_{time_frame} where target != "None"'),
                con=conn
            )

            self.X, self.y, self.id = df['text'], df['target'].apply(map_target_to_label), df['user_id']
            
        elif task == 'stance_training':
            if not target:
                 raise ValueError("You need to specify a target.")

            try:
                 df = pd.read_sql(
                     sql=f'select text, stance from tweets where target = "{target}"',
                     con=conn
                 )

            except:
                 raise DatabaseError(f'{target} is not a valid target.')

            self.X, self.y = df['text'], df['stance'].apply(int)

        elif task == 'stance_testing':
            if not target:
                raise ValueError("You need to specify a target.")
            
            try:
                df = pd.read_sql(
                    sql=(f'select text, stance, user_id from profiles_oct_nov where target = "{target}" '
                         f'union select text, stance, user_id from profiles_dec_jan where target = "{target}" '
                         f'union select text, stance, user_id from profiles_feb_mar where target = "{target}" '
                         f'union select text, stance, user_id from trump_oct_nov where target = "{target}" '
                         f'union select text, stance, user_id from trump_dec_jan where target = "{target}" '
                         f'union select text, stance, user_id from trump_feb_mar where target = "{target}"'),
                    con=conn
                )
            except:
                raise DatabaseError(f'{target} is not a valid target.')

            self.X, self.y, self.id = df['text'], df['stance'].apply(int), df['user_id']

        elif task == 'stance_testing_profiles':
            if not target:
                raise ValueError("You need to specify a target.")
            
            try:
                df = pd.read_sql(
                    sql=(f'select text, stance, user_id from profiles_oct_nov where target = "{target}" '
                         f'union select text, stance, user_id from profiles_dec_jan where target = "{target}" '
                         f'union select text, stance, user_id from profiles_feb_mar where target = "{target}" '
                         ),
                    con=conn
                )
            except:
                raise DatabaseError(f'{target} is not a valid target.')

            self.X, self.y, self.id = df['text'], df['stance'].apply(int), df['user_id']
            
        elif task == 'stance_testing_trump':
            if not target:
                raise ValueError("You need to specify a target.")
            
            try:
                df = pd.read_sql(
                    sql=(f'select text, stance, user_id from trump_oct_nov where target = "{target}" '
                         f'union select text, stance, user_id from trump_dec_jan where target = "{target}" '
                         f'union select text, stance, user_id from trump_feb_mar where target = "{target}"'),
                    con=conn
                )
            except:
                raise DatabaseError(f'{target} is not a valid target.')

            self.X, self.y, self.id = df['text'], df['stance'].apply(int), df['user_id']
            
        elif task == 'stance_testing_over_time':
            
            if not target:
                raise ValueError("You need to specify a target.")
            
            if not time_frame:
                raise ValueError("You need to specify a time frame.")
                
            if time_frame not in {"oct_nov", "dec_jan", "feb_mar"}:
                raise ValueError(f"{time_frame} is not a valid time frame.")
                
            try:
                df = pd.read_sql(
                    sql=(f'select text, stance, user_id from profiles_{time_frame} where target = "{target}" '
                         f'union select text, stance, user_id from trump_{time_frame} where target = "{target}"'),
                    con=conn
                )
            except:
                raise DatabaseError(f'{target} is not a valid target.')

            self.X, self.y, self.id = df['text'], df['stance'].apply(int), df['user_id']
            
        elif task == 'metoo_training':
            df = pd.read_sql(
                sql='select text, stance from metoo',
                con=conn
            )

            self.X, self.y = df['text'], df['stance']

        elif task == 'metoo_testing':
            df = pd.read_sql(
                sql='select text, stance, user_id from metoo_tweets_from_profiles',
                con=conn
            )

            self.X, self.y, self.id = df['text'], df['stance'], df['user_id']

        else:
            raise ValueError("Unknown training task.")

        self.X = self.X.apply(remove_urls)
        
        if mode == "word_embeddings":
            max_len = determine_max_length(task, target)

            self.X = self.X.apply(
                map_text_to_ascii_charlist_and_pad_it,
                args=(max_len, )
            )

            self.word_index = build_word_index(root, self.X)

            self.word_embeddings = load_embeddings(root, self.word_index)

            self.X = map_charlist_to_index(self.X, self.word_index)

        elif mode == "bag_of_words":
            self.X = self.X.apply(
                map_text_to_ascii_table
            )

        else:
            raise ValueError("Unknown input mode.")

        self.len = len(df)

    def __getitem__(self, index):
        if hasattr(self, 'id'):
            return self.X[index], self.y[index], self.id[index]
        else:
            return self.X[index], self.y[index]

    def __len__(self):
        return self.len
    
class MeTooDataSubset(MeTooDataset):
    """Subset of a #MeToo dataset. This is very (!!) hacky. Only reason
    this exists is that it was easier to hack this than to rebuild
    models.SentenceEmbeddings to work on subsets of a dataset."""
    def __init__(self, root, indices, mode, task, target=None, time_frame=None):
        superset = MeTooDataset(root, mode, task, target)
        
        self.X = pd.Series([superset.X.loc[idx] for idx in indices])
        self.y = pd.Series([superset.y.loc[idx] for idx in indices])
        
        if hasattr(superset, 'id'):
            self.id = pd.Series([superset.id.loc[idx] for idx in indices])
            
        self.word_index = superset.word_index
        self.word_embeddings = superset.word_embeddings 
            
        self.len = len(self.X)

    def __getitem__(self, index):
        if hasattr(self, 'id'):
            return self.X[index], self.y[index], self.id[index]
        else:
            return self.X[index], self.y[index]

    def __len__(self):
        return self.len
    