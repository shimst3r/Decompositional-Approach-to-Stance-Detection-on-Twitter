from collections import Counter
from sklearn.decomposition import PCA
from sklearn.svm import LinearSVC


def compute_sentence_embeddings(datasets, a=0.001):
    """Given one or multiple MeTooDatasets,
    this function
    computes sentence embeddings based on
    https://openreview.net/forum?id=SyK00v5xx
    """

    corpus = []

    for dataset in datasets:
        corpus.extend(dataset.X)

    word_frequencies = Counter(
        word.item()
        for sentence in corpus
        for word in sentence
    )

    word_probabilites = {
        word: word_frequencies[word]/sum(word_frequencies.values())
        for word in word_frequencies.keys()
    }

    sentence_embeddings = []

    for sentence in corpus:
        embedding = sum(
                [
                    (a/(a + word_probabilites[word.item()])
                     * datasets[0].word_embeddings[word.item()])
                    for word in sentence
                    ]
                ) / len(sentence)

        sentence_embeddings.append(embedding)

    pca = PCA(n_components=1)

    pca.fit(sentence_embeddings)

    u = pca.components_[0]

    for embedding in sentence_embeddings:
        embedding = embedding - u * u.transpose() * embedding

    return sentence_embeddings


def train_and_predict(training_dataset, testing_dataset):
    sentence_embeddings = compute_sentence_embeddings(
        [
            training_dataset,
            testing_dataset
        ]
    )

    svm = LinearSVC(
            max_iter=100000,
            verbose=1
    )

    svm.fit(
            sentence_embeddings[:len(training_dataset)],
            training_dataset.y
    )

    predictions = svm.predict(
            sentence_embeddings[len(training_dataset):],
    )

    return predictions
