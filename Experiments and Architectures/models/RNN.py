import torch

import torch.nn as nn
import torch.nn.functional as F

from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

class RNN(nn.Module):

    def __init__(
        self,
        vocab_size,
        embedding_dim,
        output_size,
        rnn_model='LSTM',
        num_layers=1,
        dropout=0.5,
        bidirectional=True
    ):
        super(RNN, self).__init__()

        # hidden_size set to embedding_dim so it can be
        # used as input for next recurrent step.
        self.hidden_size = embedding_dim

        # Store rnn_model so one can use it in forward()
        self.rnn_model = rnn_model

        self.embedding = nn.Embedding(
            num_embeddings=vocab_size,
            embedding_dim=embedding_dim
        )

        if rnn_model == 'LSTM':
            self.rnn = nn.LSTM(
                input_size=embedding_dim,
                hidden_size=self.hidden_size,
                num_layers=num_layers,
                dropout=dropout,
                batch_first=True,
                bidirectional=bidirectional
            )

        elif rnn_model == 'GRU':
            self.rnn = nn.GRU(
                input_size=embedding_dim,
                hidden_size=self.hidden_size,
                num_layers=num_layers,
                dropout=dropout,
                batch_first=True,
                bidirectional=bidirectional
            )

        else:
            raise ValueError(f'{rnn_model} is unsupported!')

        self.fc = nn.Linear(
            in_features=self.hidden_size,
            out_features=output_size
        )

        self.log_softmax = nn.LogSoftmax()

    def init_weights(self, word_vectors):
        self.embedding.weights = nn.Parameter(
            torch.from_numpy(
                word_vectors
            ).float()
        )

        self.embedding.weights.requires_grad = False

    def forward(self, sentence, lengths):
        # (b_size, s_len) -> (b_size, s_len, emb_dim)
        x = self.embedding(sentence)

        packed_x = pack_padded_sequence(
            x,
            lengths,
            batch_first=True
        )

        # initial hidden state, not implemented yet
        h_0 = None

        if self.rnn_model == 'GRU': 
            _, h_n = self.rnn(
                packed_x,
                h_0
            )

        elif self.rnn_model == 'LSTM':
            _, (h_n, c_n) = self.rnn(
                packed_x,
                h_0
            )

        x = self.fc(h_n[-1])

        return self.log_softmax(x)
